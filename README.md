# Enarx Infrastructure

This container is the center of the Enarx infrastructure. We use Wyrcan for
booting containers. Our containers are as follows:

  * dev: container with all Enarx dependencies
  * gha: the GitHub Actions runner
  * lab: common to all infra machines
  * sgx: a lab container with SGX kernel support
  * snp: a lab container with SNP kernel support
  * kvm: a lab container with KVM kernel support (stock debian kernel)

The dependencies between these containers is as follows:

```mermaid
graph TD
    dev --> gha
    dev --> lab
    lab --> sgx
    lab --> snp
    lab --> kvm
```

Pushing a change to any container causes all dependant containers in the chain
to be built.

# Making Changes

If you need to change the enarx infrastructure, please file a Merge Request
against the appropriate container. Once your merge request is merged,
we will reboot the lab systems to pick up the new settings.

## Lab Machines

If you would like access to the Enarx infrastructure, please add yourself to
**both** `users.conf` and `sshkeys.conf`. Once the lab machines have been
rebooted to pick up the changes, you can log into them using ssh with your
ssh key. For example:

```
$ ssh USERNAME@icelake.sgx.lab.enarx.dev

OR

$ ssh USERNAME@milan.sev.lab.enarx.dev
```
